#!/usr/bin/perl
use 5.014;
use utf8;

use LWP::UserAgent;
use Encode;

my $ua = LWP::UserAgent->new(env_proxy => 1);

# Fetch list of departments in country page
my $country_url = 'https://www.resultats-elections.interieur.gouv.fr/legislatives-2022/index.html';
my $country_res = $ua->get($country_url);
unless ($country_res->is_success) {
  die "Error fetching country page ($country_url): " . $country_res->code . ": " . $country_res->message . "\n";
}
my $country_content = $country_res->content;
if ($country_content =~ /charset=iso-8859-15/) {
  $country_content = Encode::decode('ISO-8859-15', $country_content);
}
my @departments = ($country_content =~ /<option value="(\d\d[\dAB])\/index\.html">/g);

# Fetch list of districts in each department page
foreach my $dep (@departments) {
  my $dep_url = "https://www.resultats-elections.interieur.gouv.fr/legislatives-2022/$dep/index.html";
  my $dep_res = $ua->get($dep_url);
  unless ($dep_res->is_success) {
    die "Error fetching department page ($dep_url): " . $dep_res->code . ": " . $dep_res->message . "\n";
  }
  my $dep_content = $dep_res->content;
  if ($dep_content =~ /charset=iso-8859-15/) {
    $dep_content = Encode::decode('ISO-8859-15', $dep_content);
  }
  my @districts = ($dep_content =~ /a href="\.\.\/$dep\/($dep\d\d)\.html">/g);

  # Fetch each district page
  foreach my $district (@districts) {
    my $district_url = "https://www.resultats-elections.interieur.gouv.fr/legislatives-2022/$dep/$district.html";
    my $district_res = $ua->get($district_url);
    unless ($district_res->is_success) {
      die "Error fetching district page ($district_url): " . $district_res->code . ": " . $district_res->message . "\n";
    }
    my $district_content = $district_res->content;
    if ($district_content =~ /charset=iso-8859-15/) {
      $district_content = Encode::decode('ISO-8859-15', $district_content);
    }
    my ($mp, $registered) = ($district_content =~ /<tr>\n<td style="text-align:left">([^<]+)<\/td>\n<td style="text-align:center">[^<]+<\/td>\n<td style="text-align:right">[\d ]+<\/td>\n<td style="text-align:center">\s*([\d,]+)<\/td>\n<td style="text-align:center">\s*[\d,]+<\/td>\n<td style="text-align:center">Oui<\/td>\n<\/tr>/s);
    print Encode::encode('UTF-8', $mp) . ": $registered%\n";
  }
}
